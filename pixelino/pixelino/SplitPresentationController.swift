//
//  SplitController.swift
//  pixelino
//
//  Created by Anshul
//

import Foundation
import UIKit

class SplitPresentationController: UIPresentationController {
    override var frameOfPresentedViewInContainerView: CGRect {
        guard let theView = containerView else {
            return CGRect.zero
        }
        return CGRect(x: 0, y: theView.bounds.height/3.0, width: theView.bounds.width, height: theView.bounds.height/1.0)
    }
}
