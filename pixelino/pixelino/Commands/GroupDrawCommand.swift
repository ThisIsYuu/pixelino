//
//  GroupDrawCommand.swift
//  pixelino
//
//  Created by Anshul
//

import Foundation

class GroupDrawCommand: Command {
    var drawCommands: Set<DrawCommand>

    init(drawCommands: Set<DrawCommand>) {
        self.drawCommands = drawCommands
    }

    init() {
        self.drawCommands = []
    }

    func execute() {
        drawCommands.forEach { $0.execute() }
    }

    func undo() {
        drawCommands.forEach { $0.undo() }
    }
}

extension GroupDrawCommand: MultiCommand {
    func appendAndExecuteSingle(_ command: Command) {
        guard let drawCommand = command as? DrawCommand else {
            return
        }
        drawCommands.insert(drawCommand)
        drawCommand.execute()
    }
}
