//
//  MultiCommand.swift
//  pixelino
//
//  Created by Anshul
//

import Foundation
protocol MultiCommand {
    func appendAndExecuteSingle(_ command: Command)
}
