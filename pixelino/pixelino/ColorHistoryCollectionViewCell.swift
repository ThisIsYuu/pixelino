//
//  ColorHistoryCollectionViewCell.swift
//  pixelino
//
//  Created by Anshul
//

import UIKit

class ColorHistoryCollectionViewCell: UICollectionViewCell {

    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
