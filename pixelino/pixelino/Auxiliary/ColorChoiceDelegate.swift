//
//  ColorChoiceDelegate.swift
//  pixelino
//
//  Created by Anshul
//

import Foundation
import UIKit

protocol ColorChoiceDelegate: class {
    func colorChoicePicked(_ color: UIColor)
}
