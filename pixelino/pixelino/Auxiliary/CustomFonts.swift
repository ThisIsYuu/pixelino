//
//  CustomFonts.swift
//  pixelino
//
//  Created by Anshul
//

import Foundation

enum CustomFonts: String {
    case roboto = "Roboto-Regular"
    case helvetica = "Helvetica"
}
