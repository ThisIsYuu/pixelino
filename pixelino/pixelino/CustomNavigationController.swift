//
//  CustomNavigationController.swift
//  pixelino
//
//  Created by Anshul
//

import UIKit

class CustomNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setCustomViewParams()
    }

    fileprivate func setCustomViewParams() {
        self.navigationBar.tintColor = .white
        self.navigationBar.barStyle = .black
        self.navigationBar.isTranslucent = false
        self.navigationBar.barTintColor = LIGHT_GREY
        self.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white,
                                                  NSAttributedString.Key.font: UIFont(
                                                    name: CustomFonts.roboto.rawValue,
                                                    size: UIFont.labelFontSize
                                                    ) ?? CustomFonts.helvetica.rawValue]
    }
}
